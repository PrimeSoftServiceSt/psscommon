﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace PssCommonLib
{
    /// <summary> 処理結果クラス
    /// </summary>
    public class ProcResult<T>
    {
        /// <summary> 処理結果
        /// </summary>
        public bool Result { get; set; } = true;
        /// <summary> メッセージ
        /// </summary>
        public string Message { get; set; } = string.Empty;
        /// <summary> 値
        /// </summary>
        public T Value { get; set; } = default(T);

        /// <summary> コンストラクタ
        /// </summary>
        /// <param name="result"></param>
        /// <param name="message"></param>
        /// <param name="t"></param>
        public ProcResult(bool result = true, string message = "", T t = default(T))
        {
            Result = result;
            Message = message;
            Value = t;
        }

        /// <summary>結果を設定する
        /// </summary>
        /// <param name="result"></param>
        /// <param name="message"></param>
        /// <param name="t"></param>
        public void SetResult(bool result, string message, T t)
        {
            SetResult(result);
            SetResult(message);
            SetResult(t);
        }

        /// <summary>結果を設定する
        /// </summary>
        /// <param name="result"></param>
        public void SetResult(bool result)
        {
            Result = result;
        }

        /// <summary> 結果を設定する
        /// </summary>
        /// <param name="message"></param>
        public void SetResult(string message)
        {
            Message = message;
        }

        /// <summary> 結果を設定する
        /// </summary>
        /// <param name="t"></param>
        public void SetResult(T t)
        {
            Value = t;
        }

    }

    /// <summary> ファイル読込処理を実装
    /// </summary>
    public static class ReadFile
    {
        /// <summary>csvファイルを読込み、string[][]を返す
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="SubDirectory"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static ProcResult<string[][]> ReadCsv(string FileName, string SubDirectory = "", Encoding encoding = null)
        {
            string strDir = Directory.GetCurrentDirectory();

            // サブディレクトリの確認
            if (SubDirectory != string.Empty)
            {
                strDir = Path.Combine(strDir, SubDirectory);
            }

            // ディレクトリ,ファイルの存在確認
            if (Directory.Exists(strDir) == false)
            {
                // ディレクトリが存在しない場合
                return new ProcResult<string[][]>(false, strDir + "が存在しません。", null);
            }
            else if (File.Exists(Path.Combine(strDir, FileName)) == false)
            {
                // ファイルが存在しない場合
                return new ProcResult<string[][]>(false, Path.Combine(strDir, FileName) + "が存在しません。", null);
            }
            else
            {
                var lst = new List<string[]>();
                using (StreamReader sr = new StreamReader(Path.Combine(strDir, FileName), encoding != null ? encoding : Encoding.GetEncoding("Shift_JIS")))
                {
                    // 読み込み
                    while (!sr.EndOfStream)
                    {
                        lst.Add(sr.ReadLine().Split(','));
                    }
                }
                return new ProcResult<string[][]>(true, string.Empty, lst.ToArray());
            }
        }
    }

    /// <summary> ファイル書き込み処理を実装
    /// </summary>
    public static class WriteFile
    {
        /// <summary> 列挙可能なTのListをCSVファイルに出力する
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename"></param>
        /// <param name="records"></param>
        /// <param name="header"></param>
        public static void WriteCsv<T>(string filename, IEnumerable<List<T>> records, string[] header = null)
        {
            using (var sw = new StreamWriter(filename, false, Encoding.GetEncoding("Shift_JIS")))
            {
                if (header != null)
                {
                    // ヘッダ書き込み
                    sw.WriteLine(string.Join(",", header));
                }
                foreach (var record in records)
                {
                    // 明細書き込み
                    sw.WriteLine(string.Join(",", record.ToArray()));
                }
            }
        }
    }

    /// <summary> Config情報の読み書き
    /// </summary>
    public static class ConfigSet
    {
        ///// <summary> Configuration取得
        ///// </summary>
        ///// <returns></returns>
        //public static Configuration GetConfiguration()
        //{
        //    return ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //}

        ///// <summary> キーから値を取得する
        ///// </summary>
        ///// <param name="Key"></param>
        ///// <returns></returns>
        //public static ProcResult<string> GetValue(string Key)
        //{
        //    var res = new ProcResult<string>() { Result = true, Message = string.Empty };
        //    try
        //    {
        //        res.Value = GetConfiguration().AppSettings.Settings[Key].Value;
        //        return res;
        //    }
        //    catch
        //    {
        //        res.SetResult(false, string.Empty, string.Empty);
        //        return res;
        //    }
        //}

        ///// <summary> キーと値のセットを保存する
        ///// </summary>
        ///// <param name="aryP"></param>
        //public static void SetValues(params (string, string)[] aryP)
        //{
        //    if (aryP.Length == 0) return;
        //    Configuration config = GetConfiguration();
        //    foreach (var p in aryP)
        //    {
        //        config.AppSettings.Settings[p.Item1].Value = p.Item2;
        //    }
        //    config.Save();
        //}
    }

    /// <summary> 拡張メソッドクラス
    /// </summary>
    public static class ExpandLogic
    {
        /// <summary>累乗した値を返す
        /// </summary>
        /// <param name="dec"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        public static decimal Pow(this decimal dec, int rank)
        {
            var res = 1m;
            if (rank < 0)
            {
                dec = 1 / dec;
            }
            foreach (var i in Enumerable.Range(0, Math.Abs(rank)))
            {
                res *= dec;
            }
            return res;
        }

        /// <summary> 少数点第n位未満の数を四捨五入する
        /// </summary>
        /// <param name="d"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        public static decimal RoundOff(this decimal d, int rank = 1)
        {
            decimal _d = 10m.Pow(rank);
            return Math.Round(d * _d, MidpointRounding.AwayFromZero) / _d;
        }

        /// <summary> 少数点第n位未満の数を切り上げする
        /// </summary>
        /// <param name="d"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        public static decimal RoundUp(this decimal d, int rank = 1)
        {
            decimal _d = 10m.Pow(rank);
            return Math.Ceiling(d * _d) / _d;
        }

        /// <summary> 少数点第n位未満の数を切り捨てする
        /// </summary>
        /// <param name="d"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        public static decimal RoundDown(this decimal d, int rank = 1)
        {
            decimal _d = 10m.Pow(rank);
            return Math.Truncate(d * _d) / _d;
        }

        /// <summary>Decimal型のTotalHoursを取得する
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        public static decimal GetDecimalTotalHours(this TimeSpan ts)
        {
            return ((decimal)ts.TotalSeconds) / (60m * 60m);
        }

        /// <summary> IEnumerable(TimeSpan)のSum
        /// </summary>
        /// <param name="spans"></param>
        /// <returns></returns>
        public static TimeSpan Sum(this IEnumerable<TimeSpan> spans)
        {
            var ts = TimeSpan.Zero;
            foreach (var span in spans)
            {
                ts += span;
            }
            return ts;
        }

        /// <summary> 文字列をTimeSpan型に変換する
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static TimeSpan ToTimeSpan(this string s)
        {
            if (TimeSpan.TryParse(s, out TimeSpan ts))
                return ts;
            else
                return TimeSpan.Zero;
        }

        /// <summary> 文字列をDateTime型に変換する
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string s)
        {
            if (DateTime.TryParse(s, out DateTime dt))
                return dt;
            else
                return new DateTime();
        }

        public static DateTime GetFirstDateOfMonth(this DateTime dt)
        {
            return dt.AddDays(1 - dt.Day);
        }

        public static DateTime GetLastDateOfMonth(this DateTime dt)
        {
            return dt.GetFirstDateOfMonth().AddMonths(1).AddDays(-1);
        }

        public static string ToStringEx(this TimeSpan ts, string format)
        {
            string hh = ts.ToString("hh");
            if (ts < TimeSpan.Zero) { hh = "-" + hh; }
            string mm = ts.ToString("mm");
            string ss = ts.ToString("ss");

            format = format.Replace("hh", hh);
            format = format.Replace("mm", mm);
            format = format.Replace("ss", ss);

            return format;
        }

        public static ProcResult<TimeSpan?> ToTimeSpanTry(this string value, string name, bool arrowEmpty = true)
        {
            var msgFormat = string.Empty;
            if (value == string.Empty)
            {
                if (arrowEmpty)
                {
                    return new ProcResult<TimeSpan?>(true, string.Empty, null);
                }
                else
                {
                    msgFormat = "{0}が未入力です。";
                    return new ProcResult<TimeSpan?>(false, string.Format(msgFormat, name), null);
                }
            }
            else
            {
                if (TimeSpan.TryParse(value, out var ts) && value.Contains(":") == true)
                {
                    return new ProcResult<TimeSpan?>(true, string.Empty, ts);
                }
                else
                {
                    msgFormat = "{0}は正しい時刻形式ではありません。";
                    return new ProcResult<TimeSpan?>(false, string.Format(msgFormat, name), ts);
                }
            }
        }

        public static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
            if (attribute != null)
            {
                return attribute.Description;
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
