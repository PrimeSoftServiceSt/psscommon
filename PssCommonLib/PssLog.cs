﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace PssCommonLib
{
    /// <summary> ログ出力クラス
    /// </summary>
    public static class PssLog
    {
        public enum LogLevel
        {
            DEBUG,
            INFO,
            WARN,
            ERROR,
            FATAL
        }

        public static void WriteLog(MethodBase method, LogLevel logLevel, string message)
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo("C:\\conf\\log4net.config"));
            ILog logger = LogManager.GetLogger(method.DeclaringType);
            var a = logger.Logger.Repository.Configured;
            switch (logLevel)
            {
                case LogLevel.DEBUG:
                    logger.Debug(message);
                    break;
                case LogLevel.INFO:
                    logger.Info(message);
                    break;
                case LogLevel.WARN:
                    logger.Warn(message);
                    break;
                case LogLevel.ERROR:
                    logger.Error(message);
                    break;
                case LogLevel.FATAL:
                    logger.Fatal(message);
                    break;
            }
        }

        public static void WriteLog(MethodBase method, LogLevel logLevel, List<string> messageList)
        {
            WriteLog(method, logLevel, string.Join(" ", messageList));
        }

        public static void WriteErrorLog(MethodBase method, Exception ex)
        {
            List<string> errorInfoList = new List<string>();
            errorInfoList.Add(ex.ToString());
            errorInfoList.Add(ex.Message);
            errorInfoList.Add(ex.StackTrace);
            WriteLog(method, LogLevel.ERROR, errorInfoList);
            if (ex.InnerException != null)
            {
                WriteErrorLog(method, ex.InnerException);
            }
        }

        /// <summary>
        /// WriteErrorLogにタスクを識別できるようにtaskCodeを追加
        /// </summary>
        /// <param name="method"></param>
        /// <param name="ex"></param>
        /// <param name="taskCode"></param>
        public static void WriteErrorLog_Task(MethodBase method, Exception ex, string taskCode)
        {
            List<string> errorInfoList = new List<string>();
            errorInfoList.Add(taskCode);
            errorInfoList.Add(ex.ToString());
            errorInfoList.Add(ex.Message);
            errorInfoList.Add(ex.StackTrace);
            WriteLog(method, LogLevel.ERROR, errorInfoList);
            if (ex.InnerException != null)
            {
                WriteErrorLog(method, ex.InnerException);
            }
        }

        public static void WriteStartLog(MethodBase method, string no)
        {
            List<string> lstMessage = new List<string>();
            lstMessage.Add(no);
            lstMessage.Add(method.Name);
            lstMessage.Add("Start");
            WriteLog(method, LogLevel.INFO, lstMessage);
        }

        public static void WriteEndLog(MethodBase method, string no)
        {
            List<string> lstMessage = new List<string>();
            lstMessage.Add(no);
            lstMessage.Add(method.Name);
            lstMessage.Add("End");
            WriteLog(method, LogLevel.INFO, lstMessage);
        }
    }
}