﻿namespace PssCommonLib
{
    /// <summary> メール送信処理を実装
    /// </summary>
    public static class PssMail
    {
        //     /// <summary> メール送信処理
        //     /// </summary>
        //     /// <param name="to"></param>
        //     /// <param name="subject"></param>
        //     /// <param name="body"></param>
        //     /// <param name="fromDisplayName"></param>
        //     /// <param name="aryAttachment"></param>
        //     /// <returns></returns>
        //     public static ProcResult<string> SendMail(MailAddress to, string subject, string body, string fromDisplayName = "", params string[] aryAttachment)
        //     {
        //try
        //{
        //             using (var msg = new MailMessage())
        //             {
        //                 // Configを読み込む
        //                 var config = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;
        //                 // From(固定)
        //                 msg.From = new MailAddress(config.Network.UserName, fromDisplayName);
        //                 // To
        //                 msg.To.Add(to);
        //                 // 件名
        //                 msg.Subject = subject;
        //                 // 本文
        //                 msg.Body = body;
        //                 // 添付ファイル(あれば)
        //                 foreach (var attachment in aryAttachment)
        //                 {
        //                     // ファイルを添付
        //                     msg.Attachments.Add(new Attachment(attachment));
        //                 }                　　
        //                 // メール送信
        //                 using (var sc = new SmtpClient())
        //                 {
        //                     sc.Send(msg);
        //                     // ログ出力
        //                     WhiteMailLog(msg);
        //                 }

        //             }
        //             return new ProcResult<string>();
        //         }
        //catch (Exception ex)
        //         {
        //             PssLog.WriteErrorLog(ex);
        //             return new ProcResult<string>(false);
        //         }
        //     }

        //     /// <summary>
        //     /// メール送信ログ
        //     /// </summary>
        //     /// <param name="mail">メール情報</param>
        //     private static void WhiteMailLog(MailMessage mail)
        //     {
        //         var sb = new StringBuilder();

        //         sb.Append(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"));
        //         sb.Append("\r\n");
        //         sb.Append("差出人：\r\n" + mail.From + "\r\n");
        //         sb.Append("受信者：\r\n" + mail.To + "\r\n");
        //         sb.Append("件名：\r\n" + mail.Subject + "\r\n");
        //         sb.Append("本文：\r\n" + mail.Body + "\r\n");
        //         foreach (var attachment in mail.Attachments)
        //         {
        //             sb.Append("添付ファイル：\r\n" + attachment.Name + "\r\n");
        //         }           

        //         var Dir = Path.Combine(Directory.GetCurrentDirectory(), "MailLog");
        //         if (!Directory.Exists(Dir))
        //         {
        //             Directory.CreateDirectory(Dir);
        //         }
        //         var path = Path.Combine(Dir, DateTime.Today.ToString("yyyyMMdd") + ".log");
        //         using (StreamWriter _sw = new StreamWriter(path, true))
        //         {
        //             _sw.WriteLine(sb.ToString());
        //         }
        //     }
    }
}