﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PssCommonLib
{
    public class PssDbaBase
    {
        private string Server { get; set; }
        private string Database { get; set; }
        private string Uid { get; set; }
        private string Pwd { get; set; }
        private string Schema { get; set; }

        protected PssDbaBase(string server, string database, string uid, string pwd, string schema)
        {
            Server = server;
            Database = database;
            Uid = uid;
            Pwd = pwd;
            Schema = schema;
        }

        /// <summary> コネクションを取得する
        /// </summary>
        /// <returns></returns>
        protected MySqlConnection GetConnection()
        {
            try
            {
                var connection = new MySqlConnection(string.Join(";", Server, Database, Uid, Pwd));
                connection.Open();
                return connection;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

        /// <summary> コマンドを取得する
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        protected MySqlCommand GetCommand(MySqlConnection connection, MySqlTransaction transaction = null)
        {
            try
            {
                var command = new MySqlCommand();
                command.Connection = connection;
                if (transaction != null)
                {
                    command.Transaction = transaction;
                }

                return command;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

        /// <summary> DBのデータを取得する(汎用)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public List<T> GetTable<T>(params Where[] aryWhere) where T : new()
        {
            try
            {
                StringBuilder sql = new StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine("    *");
                sql.AppendLine("FROM");
                sql.AppendLine("    {0}." + $"{typeof(T).Name}");
                sql.AppendLine("WHERE 1 = 1");

                int prmNo = 0;
                var lstPrm = new List<KeyValuePair<string, object>>();
                foreach (var where in aryWhere)
                {
                    string prmName = "?prm" + prmNo.ToString();
                    sql.AppendLine($"    AND {where.ColName} {where.CompareType.GetDescription()} {prmName}");
                    lstPrm.Add(new KeyValuePair<string, object>(prmName, where.Value));
                    prmNo++;
                }
                return GetTable<T>(sql, lstPrm);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

        /// <summary> DBのデータを取得する(汎用)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="lstWhere"></param>
        /// <returns></returns>
        public List<T> GetTable<T>(List<Where> lstWhere) where T : new()
        {
            try
            {
                return GetTable<T>(lstWhere.ToArray());
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

        /// <summary> DBのデータを取得する(汎用)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="lstPrm"></param>
        /// <returns></returns>
        public List<T> GetTable<T>(StringBuilder sql, List<KeyValuePair<string, object>> lstPrm = null) where T : new()
        {
            try
            {
                DataTable tbl = new DataTable();
                using (var connection = GetConnection())
                using (var command = GetCommand(connection))
                {
                    command.CommandText = string.Format(sql.ToString(), Schema);
                    if (lstPrm != null && lstPrm.Count > 0)
                    {
                        lstPrm.ForEach(x => command.Parameters.AddWithValue(x.Key, x.Value));
                    }
                    using (var reader = command.ExecuteReader())
                    {
                        tbl.Load(reader);
                    }
                    return ConvertTo<T>(tbl);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

        /// <summary> 取得したデータテーブルをTのリストに変換する
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="datatable"></param>
        /// <returns></returns>
        private List<T> ConvertTo<T>(DataTable datatable) where T : new()
        {
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);

                return datatable.AsEnumerable().ToList().ConvertAll<T>(row => GetObject<T>(row, columnsNames));
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

        /// <summary> 要素を取得,変換する
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="columnsName"></param>
        /// <returns></returns>
        private T GetObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                if (objProperty.PropertyType.ToString() == "System.Boolean")
                                {
                                    value = value == "0" ? "false" : "true";
                                }
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw;
            }
        }

    }

    /// <summary> 条件クラス
    /// </summary>
    public class Where
    {
        public string ColName;
        public object Value;
        public COMPARE_TYPE CompareType;
        public Where(string colName, object value, COMPARE_TYPE compareType)
        {
            ColName = colName;
            Value = value;
            CompareType = compareType;
        }
        public Where(string colName, object value)
        {
            ColName = colName;
            Value = value;
            CompareType = COMPARE_TYPE.EQUAL;
        }
    }

    /// <summary> 比較タイプ
    /// </summary>
    public enum COMPARE_TYPE
    {
        [Description("=")]
        EQUAL,

        [Description(">")]
        LARGER,

        [Description(">=")]
        LARGER_EQUAL,

        [Description("<")]
        LOWER,

        [Description("<=")]
        LOWER_EQUAL,

        [Description("<>")]
        NOT_EQUAL
    }
}
