﻿using ClosedXML.Excel;
using System;
using System.IO;
using System.Reflection;

namespace PssCommonLib
{
    /// <summary>
    /// Excelの読み込みとワークシート、セルへのアクセスを行うクラス
    /// </summary>
    public class ExcelLoader : IDisposable
    {
        #region メンバ変数
        /// <summary>
        /// Excelのワークブック(2013以降のExcelを指定すること)
        /// </summary>
        private XLWorkbook _Workbook = null;
        #endregion

        #region プロパティ
        /// <summary>
        /// 全てのワークシートを取得します。
        /// </summary>
        public IXLWorksheets Worksheets
        {
            get
            {
                if (this._Workbook != null)
                {
                    return this._Workbook.Worksheets;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// 新しいクラスのインスタンスを作成します。
        /// </summary>
        public ExcelLoader()
        {
        }

        /// <summary>
        /// 新しいクラスのインスタンスを作成します。
        /// </summary>
        /// <param name="path">Excelファイルのパス</param>
        public ExcelLoader(string path)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    // ファイルパスが設定されているかつ、そのパスのファイルが存在する場合、ファイルを開く
                    this._Workbook = new XLWorkbook(path);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }
        #endregion

        #region 公開メソッド
        /// <summary>
        /// 解放処理
        /// </summary>
        public void Dispose()
        {
            CloseExcel();
        }

        /// <summary>
        /// 引数で指定されたExcelファイルを開きます。
        /// </summary>
        /// <param name="path">Excelファイルのパス</param>
        /// <returns>true：オープン成功、false：オープン失敗</returns>
        public bool OpenExcel(string path)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
            CloseExcel();

            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                // ファイルパスが設定されているかつ、そのパスのファイルが存在する場合、ファイルを開く
                this._Workbook = new XLWorkbook(path);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 指定したインデックスのワークシートの指定セルを読み込みます。
        /// </summary>
        /// <param name="row">行番号</param>
        /// <param name="col">列番号</param>
        /// <param name="index">ワークシートのインデックス番号</param>
        /// <returns>true：オープン成功、false：オープン失敗</returns>
        public string ReadCell(int row, int col, int index = 1)
        {
            if (this._Workbook == null)
            {
                return string.Empty;
            }

            try
            {
                var worksheet = this._Workbook.Worksheet(index);
                var cell = worksheet.Cell(row, col);
                return cell.Value as string;
            }
            catch
            {
                // 指定されたワークシートが存在しなかった場合、空値を返却する
                return string.Empty;
            }
        }

        /// <summary>
        /// 指定したワークシートの指定セルを読み込みます。
        /// </summary>
        /// <param name="row">行番号</param>
        /// <param name="col">列番号</param>
        /// <param name="worksheet">ワークシートのインデックス番号</param>
        /// <returns>true：オープン成功、false：オープン失敗</returns>
        public string ReadCell(int row, int col, IXLWorksheet worksheet)
        {
            if (worksheet == null)
            {
                return string.Empty;
            }

            try
            {
                var cell = worksheet.Cell(row, col);
                return cell.Value as String;
            }
            catch
            {
                // 指定されたワークシートのセルが存在しなかった場合、空値を返却する
                return string.Empty;
            }
        }

        /// <summary>
        /// 現在オープンされているExcelファイルをクローズします。
        /// </summary>
        public void CloseExcel()
        {
            if (this._Workbook != null)
            {
                // 既にExcelが開かれている場合、破棄する
                this._Workbook.Dispose();
                this._Workbook = null;
            }
        }
        #endregion

        #region 公開メソッド

        /// <summary> Excel出力（印刷）
        /// </summary>
        /// <returns></returns>
        public MemoryStream PrintExcel()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                var stream = new MemoryStream();
                // 前処理(あれば)
                var resBefore = Before();
                if (resBefore == false)
                    return null;

                // メイン処理
                var resMain = Main();
                if (resMain == false)
                    return null;

                _Workbook.SaveAs(stream);

                // 後処理(あれば)
                var resAfter = After();
                if (resAfter == false)
                    return null;

                return stream;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                return null;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
                GC.Collect();
            }
        }

        #endregion

        #region EXCEL編集処理

        /// <summary> 前処理(必要ならoverride)
        /// </summary>
        /// <returns></returns>
        protected virtual bool Before()
        {
            return true;
        }

        /// <summary> メイン処理
        /// </summary>
        /// <returns></returns>
        protected virtual bool Main()
        {
            return true;
        }

        /// <summary> 後処理(必要ならoverride)
        /// </summary>
        /// <returns></returns>
        protected virtual bool After()
        {
            return true;
        }

        /// <summary> セルに値を設定する
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="value"></param>
        protected void SetValue(int row, int col, object value, int sheetIndex = 1)
        {
            if (value != null)
            {
                var worksheet = this._Workbook.Worksheet(sheetIndex);
                worksheet.Cell(row, col).Value = value;
            }
        }

        /// <summary> 書式,値のクリア
        /// </summary>
        /// <param name="iRow"></param>
        /// <param name="iCol"></param>
        protected void SetCleer(int iRow, int iCol, int sheetIndex = 1)
        {
            var worksheet = this._Workbook.Worksheet(sheetIndex);
            worksheet.Cell(iRow, iCol).Style.Border.TopBorder = XLBorderStyleValues.None;
            worksheet.Cell(iRow, iCol).Style.Border.RightBorder = XLBorderStyleValues.None;
            worksheet.Cell(iRow, iCol).Style.Border.LeftBorder = XLBorderStyleValues.None;
            worksheet.Cell(iRow, iCol).Style.Border.BottomBorder = XLBorderStyleValues.None;
            worksheet.Cell(iRow, iCol).Style.Fill.BackgroundColor = XLColor.NoColor;
            worksheet.Cell(iRow, iCol).Value = string.Empty;
        }

        protected void SetFormat(int iRow, int iCol, string format, int sheetIndex = 1)
        {
            var worksheet = this._Workbook.Worksheet(sheetIndex);
            worksheet.Cell(iRow, iCol).Style.NumberFormat.Format = format;
        }
        #endregion
    }
}