﻿namespace PssCommonLib
{
    /// <summary> メッセージボックス
    /// </summary>
    public static class PssMsg
    {
        ///// <summary> エラーメッセージ表示
        ///// </summary>
        ///// <param name="message"></param>
        ///// <param name="caption"></param>
        ///// <returns></returns>
        //public static DialogResult ShowErrorMessage(string message, string caption = "エラー")
        //{
        //    return MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //}
        ///// <summary> 例外メッセージ表示
        ///// </summary>
        ///// <returns></returns>
        //public static DialogResult ShowExceptionMessage()
        //{
        //    return ShowErrorMessage("エラーが発生しました。ログを確認してください。", "Error!");
        //}
        ///// <summary> 確認メッセージ表示
        ///// </summary>
        ///// <param name="message"></param>
        ///// <param name="caption"></param>
        ///// <param name="button"></param>
        ///// <returns></returns>
        //public static DialogResult ShowQuestinMessage(string message, string caption = "確認", MessageBoxButtons button = MessageBoxButtons.OKCancel)
        //{
        //    return MessageBox.Show(message, caption, button, MessageBoxIcon.Question);   
        //}
        ///// <summary> 警告メッセージ表示
        ///// </summary>
        ///// <param name="message"></param>
        ///// <param name="button"></param>
        ///// <returns></returns>
        //public static DialogResult ShowWorningMessage(string message, MessageBoxButtons button = MessageBoxButtons.OKCancel)
        //{
        //    return MessageBox.Show(message, "警告", button, MessageBoxIcon.Warning);
        //}
        ///// <summary> 完了メッセージ表示
        ///// </summary>
        ///// <param name="procName"></param>
        ///// <returns></returns>
        //public static DialogResult ShowCompleteMessage(string procName)
        //{
        //    return MessageBox.Show(string.Format("{0}が完了しました。", procName), "完了", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //}
        ///// <summary> 失敗メッセージ表示
        ///// </summary>
        ///// <param name="procName"></param>
        ///// <returns></returns>
        //public static DialogResult ShowFalseMessage(string procName)
        //{
        //    return ShowErrorMessage(string.Format("{0}が失敗しました。", procName), "失敗");
        //}
    }
}
