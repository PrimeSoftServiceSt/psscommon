﻿namespace PssCommonLib
{
    /// <summary>
    /// ファイル拡張子
    /// </summary>
    public struct Extention
    {
        /// <summary>.bin
        /// </summary>
        public const string BIN = ".bin";

        /// <summary>.csv
        /// </summary>
        public const string CSV = ".csv";

        /// <summary>.xlsx
        /// </summary>
        public const string XLSX = ".xlsx";
    }
}
